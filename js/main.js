/**
 * The main JS file for my_custom_module.
 */

((Drupal) => {
  Drupal.behaviors.myCustomModule = {
    attach: (context, settings) => {
      const { message } = settings.myCustomModule;

      once("myCustomModule", "main", context).forEach(() => {
        console.log(message);
      });
    },
  };
})(Drupal);

